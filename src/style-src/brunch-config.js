module.exports = {
  paths: {
    public: "../static/"
  },
  sourceMaps: false,
  files: {
    stylesheets: { joinTo: "app.css" }
  },
  plugins: {
    less: {},
    postcss: {
      processors: [
        // compatibility prefixes
        require("autoprefixer")(),
        // removes unused styles
        require("csswring")(),
        // minimize
        require("cssnano")({ preset: "default" })
      ]
    }
  }
};
