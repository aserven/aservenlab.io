{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad         (forM)
import           Data.List             (isInfixOf, sortBy)
import           Data.Monoid           ((<>))
import           Data.Ord              (comparing)
import           Data.Time.Format      (defaultTimeLocale)
import           GHC.IO.Encoding       (setFileSystemEncoding,
                                        setForeignEncoding, setLocaleEncoding,
                                        utf8)
import           Hakyll
import           System.FilePath.Posix (splitFileName, takeBaseName,
                                        takeDirectory, (</>))

--------------------------------------------------------------------------------
main :: IO ()
main = do
    setLocaleEncoding utf8
    setFileSystemEncoding utf8
    setForeignEncoding utf8
    hakyllWith config $ do
        match ("static/**" .||. "favicon.ico" .||. "robots.txt" .||.
               "posts/**.png" .||. "posts/**.jpg" .||. "quarter-brain/**" .||.
               "keybase.txt") $ do
            route   idRoute
            compile copyFileCompiler

        -- match "posts/*" $ do
            -- route niceRoute
            -- compile $ pandocCompiler
                -- >>= saveSnapshot "post"
                -- >>= loadAndApplyTemplate "templates/post.html"    postCtx
                -- >>= loadAndApplyTemplate "templates/default.html" postCtx
                -- >>= relativizeUrls
                -- >>= removeIndexHtml

        -- create ["feed.xml"] $ do
            -- route idRoute
            -- compile $
                -- loadAllSnapshots "posts/*" "post"
                -- >>= fmap (take 10) . createdFirst
                -- >>= renderAtom feedConfiguration feedCtx

        match "index.html" $ do
            route idRoute
            compile $ do
                let projectsCtx =
                        constField "active-about" "y" <>
                        constField "title" "About"    <>
                        defaultContext
                getResourceBody
                    >>= applyAsTemplate projectsCtx
                    >>= loadAndApplyTemplate "templates/default.html" projectsCtx
                    >>= relativizeUrls
                    >>= removeIndexHtml
            -- compile $ do
            --     let posts = recentFirst =<< loadAllSnapshots "posts/*" "post"
            --     let blogCtx =
            --             listField "posts" postCtx posts <>
            --             constField "active-blog" "n"    <>
            --             defaultContext
            --     getResourceBody
            --         >>= applyAsTemplate blogCtx
            --         >>= loadAndApplyTemplate "templates/default.html" blogCtx
            --         >>= relativizeUrls
            --         >>= removeIndexHtml

        -- match "projects.html" $ do
            -- route niceRoute
            -- compile $ do
                -- let projectsCtx =
                        -- constField "active-projects" "n" <>
                        -- constField "title" "Projects"    <>
                        -- defaultContext
                -- getResourceBody
                    -- >>= applyAsTemplate projectsCtx
                    -- >>= loadAndApplyTemplate "templates/default.html"
                        -- projectsCtx
                    -- >>= relativizeUrls
                    -- >>= removeIndexHtml

        -- match "about.html" $ do
            -- route niceRoute
            -- compile $ do
                -- let projectsCtx =
                        -- constField "active-about" "y" <>
                        -- constField "title" "About"    <>
                        -- defaultContext
                -- getResourceBody
                    -- >>= applyAsTemplate projectsCtx
                    -- >>= loadAndApplyTemplate "templates/default.html"
                        -- projectsCtx
                    -- >>= relativizeUrls
                    -- >>= removeIndexHtml

        match "404.html" $ do
            route idRoute
            compile $
                getResourceBody
                    >>= applyAsTemplate defaultContext
                    >>= loadAndApplyTemplate "templates/default.html"
                        defaultContext
                    >>= relativizeUrls
                    >>= removeIndexHtml

        match "templates/*" $ compile templateCompiler


postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" <>
    constField "active-blog" "y" <>
    ertField "ert"               <>
    metaKeywordCtx               <>
    defaultContext


metaKeywordCtx :: Context String
metaKeywordCtx = field "metaKeywords" $ \item -> do
    tags <- getMetadataField (itemIdentifier item) "tags"
    return $ maybe "" showMetaTags tags
    where
        showMetaTags t = "<meta name=\"keywords\" content=\"" ++ t ++ "\"/>\n"


-- estimated reading time
ertField :: String -> Context String
ertField key = field key $ \item ->
  return . wordsToMinutes . length . words . stripTags $ itemBody item


wordsToMinutes :: Int -> String
wordsToMinutes = toMinutesString . ceiling . minPerWord
  where
    -- average reading speed 250 words per minute
    minPerWord x = fromIntegral x / 250 :: Double


toMinutesString :: Int -> String
toMinutesString x = show x ++ " minutes"


feedCtx :: Context String
feedCtx = defaultContext <> bodyField "description"


feedConfiguration :: FeedConfiguration
feedConfiguration = FeedConfiguration
    { feedTitle = "aserven.gitlab.io blog"
    , feedDescription = "Personal blog"
    , feedAuthorName = "Albert Serven"
    , feedAuthorEmail = "albert@serven,cat"
    , feedRoot = "https://aserven.gitlab.io"
    }


niceRoute :: Routes
niceRoute = customRoute createIndexRoute
    where
        createIndexRoute ident =
            takeDirectory path </> takeBaseName path </> "index.html"
            where path = toFilePath ident


removeIndexHtml :: Item String -> Compiler (Item String)
removeIndexHtml item = return $ fmap (withUrls removeIndexStr) item
    where
        removeIndexStr :: String -> String
        removeIndexStr url = case splitFileName url of
            (dir, "index.html") | isLocal dir -> dir
            _                   -> url
        isLocal uri = not ("://" `isInfixOf` uri)


config :: Configuration
config = defaultConfiguration
    { destinationDirectory = "../public"
    }


createdFirst :: [Item String] -> Compiler [Item String]
createdFirst items = do
    itemsWithTime <- forM items $ \item -> do
        utc <- getItemUTC defaultTimeLocale $ itemIdentifier item
        return (utc,item)
    return $ map snd $ sortBy (flip $ comparing fst) itemsWithTime

