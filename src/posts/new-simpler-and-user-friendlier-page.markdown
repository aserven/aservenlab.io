---
title: New, simpler and user-friendlier page
tags: user-hostile web, hakyll, pure.css, brunch, css, elm
published: 2018-12-11
---

As we say in Czech "blacksmith's mare walks barefoot" (or "the shoemaker's children go barefoot" in English), my previous page with its Bootstrap based style looked admittedly so terrible that I myself didn't enjoy looking at it anymore. 

I finally found some time to give it much needed revamp. Also, many current developments in our industry are, to put it lightly, less than ideal and I wanted to make sure I am not just adding more fuel to the fire. It's not just the things we can see straight away[^1], which are easier to laugh off, but more importantly the things that are hidden from most people's sight[^2]. In that spirit, all the content on this site is now self-hosted (well, except for the comments, 🤔, maybe I'll setup something like [this self-hosted commenting system](https://github.com/Libbum/oration) one day).

[^1]: [Daryl Ginn on Twitter: "Every website in 2018…"](https://twitter.com/darylginn/status/1053646859686809600)

[^2]: [Against an Increasingly User-Hostile Web](https://www.neustadt.fr/essays/against-a-user-hostile-web/)

To keep things simple, without making it [too good](https://bestmotherfucking.website/), I've used [Pure.css](https://purecss.io/) with its small, responsive modules. It is nice, but quite frankly, even though I've been making websites since my childhood, going back to CSS has been a total pain in the ass (even for something as simple as this website) after being spoilt with the awesome [elm-ui](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/) (previously style-elements) that I've been using at work for well over a year. But the best thing about using Elm has really been its welcoming, friendly and inclusive community.

I've also added a simple [way to estimate reading time](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/blob/master/src/Main.hs#L117) for the blog posts. If you're interested, you can see the source for this page on [GitLab](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io). Feel free to use any of it for your own page.
