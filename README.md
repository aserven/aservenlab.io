Albert Serven's page
=============


Source code for [aserven.gitlab.io](https://aserven.gitlab.io).

This page is built with [Hakyll](https://jaspervdj.be/hakyll/) and [Brunch](https://brunch.io/) with [Pure.css](https://purecss.io/).

Brunch is used to build the stylesheets (from the [src/style-src](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/tree/master/src/style-src) directory), while [Hakyll](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/blob/master/src/Main.hs) runs Pandoc on the markdown posts sources to render them into HTML and then puts everything together.

## Build Stylesheet with Brunch

[Install brunch](https://brunch.io/docs/getting-started#tasting-your-first-brunch)

```
cd src/style-src
brunch build --production
```

## Build Blog Hakyll

[Install stack](https://docs.haskellstack.org/en/stable/install_and_upgrade/)

```
cd src
stack install
stack build
stack exec site build
```

## Develop (run watch on Brunch and Hakyll sources)

```
cd src/style-src
brunch watch
```

and

```
cd src
stack exec site watch
```

## Publish

Push to GitLab, [CI](.gitlab-ci.yml) will do the rest.
